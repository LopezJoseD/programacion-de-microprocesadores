/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/11
 * Version: 2.0
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/
#include <stdbool.h>
#include <stdio.h>
#include <practicauno.h>
#include <sapi.h>

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

int main( void )
{
	// ----- Setup -----------------------------------
	boardInit(); ///< Board initialization

	delay_t delay; ///< Not blocking delay variable declaration
	delayConfig( &delay, 500 ); ///< Set delay to 500ms

	const int ledqty = 3; ///< Constant integer that specify leds quantity
	int leds[3] = {LED1,LED2,LED3}; ///< Array variable with ints values of pins following schematic detail

	int i = ledqty; //< declaration of iterator variable for set led to be on
	int k = 1; ///< Iterator variable that goes by every led to action on or off

	bool seq = TRUE; ///< boolean value that sets sequence forward or backward

	// ----- Repeat for ever -------------------------
	while( TRUE ) {
		if ( !gpioRead( TEC1 ) ){
			seq = !seq; ///< switching sequence
         printf("Cambio de sentido! \n");
		}///< checks if the button was pushed to switch sequence


		if ( delayRead( &delay ) ){
			if ( seq ){
				i++; ///< increasing iterator i to go forward
            printf("Vamos hacia adelante \n");
			}
			else{
				i--; ///< decrease iterator i to go backward
            printf("Vamos hacia atras \n");
			}
		} ///< if the timer was complete it increase or decrease iterator i

		if ( i>ledqty ){
			i = 1; ///< set iterator i to the bottom value
		} ///< checks if iterator i overflow ledsqty
		if ( i < 1 ) {
			i = ledqty; ///< set value of i to ledsqty
		}///< checks if iterator i goes to 0

		for (k=1;k<=ledqty;k++){
			if (k==i){
				gpioWrite( leds[k-1], ON );///< set led on
			}
			else{
				gpioWrite( leds[k-1], OFF );///< set led off
			}
		}///< loops every led to turn on or off according to iterator i

	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
