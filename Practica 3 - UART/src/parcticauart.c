/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/04/22
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "parcticauart.h"
#include "sapi.h"
#include "string.h"

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

int main( void )
{
   // ----- Setup -----------------------------------
   boardInit();

   // Inicializar UART_USB a 115200 baudios
   uartConfig( UART_USB, 115200 );
   gpioWrite(LED_GREEN,1);

   uint8_t receivedByte  = 0;
   int8_t ledvalue = 0;
   char initText[] = "UART Initialized OK\r\n";
   char message[] = "UART Initialized OK\r\n";

   /* Buffer */
   static char uartBuff[10];
   uartWriteString( UART_USB, initText );



   // ----- Repeat for ever -------------------------
   while( true ) {
      // Si recibe un byte de la UART_USB lo guardarlo en la variable dato.
      if(  uartReadByte( UART_USB, &receivedByte ) ){
         switch (receivedByte)
         {
         case '1':
            gpioToggle(LED1);
            uartWriteString( UART_USB, "LED 1 value toggled \r\n");
            break;
         case '2':
            gpioToggle(LED2);
            uartWriteString( UART_USB, "LED 2 value toggled \r\n");
            break;
         case '3':
            gpioToggle(LED3);
            uartWriteString( UART_USB, "LED 3 value toggled \r\n");
            break;
         case '\n':
            break;
         default:
            uartWriteString( UART_USB, "Please insert options 1, 2, or 3\r\n" );
            break;
         }
         // Se reenvia el dato a la UART_USB realizando un eco de lo que llega
         // uartWriteByte( UART_USB, receivedByte );
      }
   }

   // YOU NEVER REACH HERE, because this program runs directly or on a
   // microcontroller and is not called by any Operating System, as in the 
   // case of a PC program.
   return 0;
}
