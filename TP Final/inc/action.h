#ifndef __ACTION_H__
#define __ACTION_H__
#include <stdint.h>
#include <stddef.h>
#include <sapi.h>

/*=====[Prototypes (declarations) of public functions]=======================*/
bool_t makeAction(int8_t output);

#endif /* __ACTION_H__ */