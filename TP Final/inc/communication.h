#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__
#include <stdint.h>
#include <stddef.h>
#include <sapi.h>
#include <meftpfinal.h>

/*=====[Prototypes (declarations) of public functions]=======================*/
void checkCommunication(mef_state_t * pMef, int8_t * pConfigCommand);

#endif /* __COMMUNICATION_H__ */