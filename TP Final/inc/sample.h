#ifndef __SAMPLE_H__
#define __SAMPLE_H__
#include <stdint.h>
#include <stddef.h>
#include <sapi.h>

typedef enum {
    ONE = 1000,
    FIVE = 5000,
    TEN = 10000
} sample_type_t;

typedef enum {
    NEW,
    WORKING,
    STOPPED
} sample_status_t;

typedef struct {
    sample_type_t mode;
    sample_status_t status;
    float saved_value;
} sample_t;


/*=====[Prototypes (declarations) of public functions]=======================*/
bool_t sampleInit(sample_t * pSample);
bool_t sampleConfigure(sample_t * pSample, int8_t sample_type);
void sampleProcess(sample_t * pSample);
char getSampleStatus(sample_t * pSample);
float getSampleSavedValue(sample_t * pSample);

#endif /* __SAMPLE_H__ */