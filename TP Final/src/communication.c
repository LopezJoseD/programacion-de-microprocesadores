/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/04/22
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/
#include "communication.h"
#include <meftpfinal.h>
#include "sapi.h"


/*=====[Definition macros of private constants]==============================*/
uint8_t receivedByte  = 0;
/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/


/** Private void function that evaluates the Request
 * It receives the text and the values of status
 * and command that must be filled according received text.
 * \param r_byte Received byte
 * \param pStatusResponse status variable that must be filled
 * \param pCommandResponse command variable that must be filled
 */ 
void evaluateRequest(uint8_t * r_byte, int8_t * pStatusResponse, int8_t * pCommandResponse){
    switch (*r_byte) ///< Evaluates byte received
    {
        case '1':
            *pStatusResponse = CONFIGURING;
            *pCommandResponse = 1;
            uartWriteString( UART_USB, "Setting new configuration to mode 1 ok \r\n");
            break;
        case '2':
            *pStatusResponse = CONFIGURING;
            *pCommandResponse = 2;
            uartWriteString( UART_USB, "Setting new configuration to mode 2 ok \r\n");
            break;
        case '3':
            *pStatusResponse = CONFIGURING;
            *pCommandResponse = 3;
            uartWriteString( UART_USB, "Setting new configuration to mode 3 ok \r\n");
            break;
        case '4':
            *pStatusResponse = ACTING;
            *pCommandResponse = 4;
            uartWriteString( UART_USB, "Configured to open doors \r\n");
            break;
        case '5':
            ///< configure close doors
            uartWriteString( UART_USB, "Configured to close doors \r\n");
            *pStatusResponse = ACTING;
            *pCommandResponse = 5;
            break;
        case '6':
            ///< Request of information and sending it.
            uartWriteString( UART_USB, "Configured to send information \r\n");
            *pStatusResponse = SENDING;
            break;
        default:
            uartWriteString( UART_USB, "Invalid Command, please check reference. \r\n" );
            uartWriteString( UART_USB, "Command received: " );
            uartWriteByte( UART_USB, *r_byte );
            uartWriteString( UART_USB, " \r\n" );
            break;
    }
};



/** boolean function Check Communication
 * This function cares about receiving commands by uart
 * It should be an interruption.
 * According to received command evaluates the request and returns
 * new state and command to excecute.
 * \param pMef FSM object to be changed
 * \param pConfigCommand command to be set
 */ 
void checkCommunication(mef_state_t * pMef, int8_t * pConfigCommand){
    if(  uartReadByte( UART_USB, &receivedByte ) ){
        switch (receivedByte)
        {
            case '\n':
                break;
            case '\r':
                break;
            default:
                ;
                int8_t status_response; ///< Declares local variable to be filled with new status
                int8_t command_response = 0; ///< Declares local variable to be filled with new command
                evaluateRequest(&receivedByte, &status_response, &command_response);    ///< Evaluates command received
                *pMef = status_response;    ///< fill FSM with newstatus
                *pConfigCommand = command_response; ///< fill command varaible with new value.
                break;
        }
    }
};

