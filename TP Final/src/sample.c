/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/04/22
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/
#include "sample.h"
#include "sapi.h"


/*=====[Definition macros of private constants]==============================*/
#define INITIAL_DEFAULT_SAMPLING_MODE ONE
#define INITIAL_DEFAULT_SAMPLING_STATUS NEW
#define INITIAL_DEFAULT_SAMPLING_VALUE 0
#define ONETIME 1
#define FIVETIME 5
#define TENTIME 3

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
int8_t i = 0;
delay_t retardo;
uint16_t read_value = 0;
float converted_value = 0;


/** static sampleProcess function
 * It's in charge of whole sample process, it is a FSM itself.
 * According to the status of the sample object it makes an new sampling,
 * save the sample, or stopp the sampling process.
 * \param pSam Sample object
 */
void sampleProcess(sample_t * pSam){
    switch (pSam->status){
        case NEW:   ///< Fresh new sample created, must be configured following sample mode.
            delayWrite(&retardo, pSam->mode);    ///< Ticking sample creationg
            pSam->status = WORKING; ///< Changes to working status
            break;
        case WORKING: ///< Working status where the sample must be done if time period is over
            if (delayRead(&retardo)){   ///< if configured time is due then it makes the sample
                read_value = adcRead( CH1 );    ///< Read information from ADC.
                converted_value = read_value*3.3/1024;  ///< Converts value readed
                pSam->saved_value = converted_value;    ///< Saves value into sample object
                pSam->status = NEW; ///< returns sample object to new one to start process over again.
                printf("Leo el valor %f \r\n", converted_value);    ///< This should be sent by UART
            }
            break;
        case STOPPED: ///< In this status there is no change or action.
            break;
    }
};


/** char function getSampleStatus
 * It returns the status of sample object received.
 * \param pSam Sample object
 * \return char Sample status
 */
char getSampleStatus(sample_t * pSam){
    return pSam->status;    ///< Returns the status of the object.
};

/** float function getSampleSavedValue
 * It returns the saved sampled value of sample object received.
 * \param pSam Sample object
 * \return float Sampled saved value
 */
float getSampleSavedValue(sample_t * pSam){
    return pSam->saved_value;   ///< Returns the value stored of the object.
};


/** boolean function for Configure a Sample
 * It returns true or false if the sample was configured ok.
 * \param pSam Sample object
 * \param sample_type Type of sample that will be changed to.
 * \return boolean with result.
 */
bool_t sampleConfigure(sample_t * pSam, int8_t sample_type){
    switch (sample_type){
        case 1:
            pSam->mode = ONE;
            uartWriteString( UART_USB, "Muestreo cada segundo ahora \r\n");
            return true;
            break;
        case 2:
            pSam->mode = FIVE;
            uartWriteString( UART_USB, "Muestreo cada cinco segundos ahora \r\n");
            return true;
            break;
        case 3:
            pSam->mode = TEN;
            uartWriteString( UART_USB, "Muestreo cada diez segundos ahora \r\n");
            return true;
            break;
        default:
            ///< There is no modification made, so return false.
            return false;
            break;
    }
};


/** boolean function Sample Init 
 * This functions creates an sampling object and returns
 * true or false according to creation.
 * Takes initial default values for creating it. 
 * \param pSam Sample object
 * \return boolean
 */ 
bool_t sampleInit(sample_t * pSam){
   
   if (NULL == pSam){   ///< Checks if there is a sample object sended
      uartWriteString( UART_USB, "There is no sample object received to init \r\n");    ///< Text with message when there is no object
      return false; ///< Return according no sucessfully process.
   }
   pSam->status = INITIAL_DEFAULT_SAMPLING_STATUS;  ///< Sample is created with default status
   pSam->mode = INITIAL_DEFAULT_SAMPLING_MODE;  ///< Sample is created with default mode
   pSam->saved_value = INITIAL_DEFAULT_SAMPLING_VALUE;  ///< Sample is created with default value
   uartWriteString( UART_USB, "Sample created OK \r\n");   ///< Confirmation message
   return true; ///< Ok return.
};