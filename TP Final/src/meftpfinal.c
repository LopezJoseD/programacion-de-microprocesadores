/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/04/22
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/
#include <string.h>
#include <stdio.h> 
#include "meftpfinal.h"
#include <sample.h>
#include <action.h>
#include <communication.h>
#include "sapi.h"

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
#define INITIAL_DEFAULT_STATE SAMPLING ///< Define initial state for machine

mef_state_t mef;  ///< Creation of FSM object
int8_t config_command;

/*=====[Main function, program entry point after power on or reset]==========*/

int main( void )
{
   /**   First running setup
    *    It inits board and ADC, also set the value for common variables
    */
   boardInit();   ///< Initiate board
   adcConfig( ADC_ENABLE );   ///< Configuring and enabling ADC
   uartConfig( UART_USB, 115200 );  ///< UART init configuration and enabling
   static char uartBuff[10];  ///< UART Buffer configuration

   sample_t sam;  ///< creation of new sample object
   mef = INITIAL_DEFAULT_STATE; ///< Set mef with INITIAL_DEFAULT_STATE

   uartWriteString( UART_USB, "FSM init ok \r\n");
   gpioWrite(LEDB,1); ///< Visual output to confirm that is working


   /** Try to create an sampling FSM
    */
   if (sampleInit(&sam)){ ///< Check if it is possible to create a new sampling object
      uartWriteString( UART_USB, "Se inicializa correctamente el sample \r\n");
   }else {
      uartWriteString( UART_USB, "Problema inicializando el sample \r\n");
      return 0;
   }

   
   /** Eternal loop that will check the differents states of finite machine.
    */
   while( true ) {
      switch (mef)   ///< Switch that evaluates states and guide the excecution according that.
      {
      case CONFIGURING: ///< Configuring state, indicates that there is a parameter that must be changed.
         /* code */
         if (sampleConfigure(&sam, config_command)){  ///< Configure sampling process.
            mef = SAMPLING;   ///< SET state to SAMPLING again.
            uartWriteString( UART_USB, "Configured OK \r\n"); /// Inform changes ok.
         }
         break;
      case SAMPLING: ///< This state will be the most used till external user makes a requirement
         if(getSampleStatus(&sam) != STOPPED){  ///< If for any reason is not stopped sampling we do sampling
            sampleProcess(&sam); ///< Sample process with it FSM itself.
         };
         break;
      case ACTING:   ///< This state happens when external user wants to change an output.
         if (makeAction(config_command)){ ///< Action procss if is ok executed
            mef = SAMPLING; ///< SET state to SAMPLING again.
            uartWriteString( UART_USB, "Action OK \r\n"); /// Messages that confirms that action was produced ok.
         }
         break;
      case SENDING:  ///< This state sends information to external user.
         printf("Last sample value stored is: %f \r\n", getSampleSavedValue(&sam)); ///< Brings value stored
         mef = SAMPLING; ///< SET state to SAMPLING again.
         break;
      default:
         break;
      }
      checkCommunication(&mef, &config_command);   ///< Communication process check. Should be an interruption.
   }

   // YOU NEVER REACH HERE, because this program runs directly or on a
   // microcontroller and is not called by any Operating System, as in the 
   // case of a PC program.
   return 0;
}