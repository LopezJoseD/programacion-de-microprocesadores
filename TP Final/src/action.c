/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/04/22
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/
#include "action.h"
#include "sapi.h"


/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/


/** boolean function Sample Init 
 * This functions creates an sampling object and returns
 * true or false according to creation.
 * Takes initial default values for creating it. 
 * \param pSam Sample object
 * \return boolean
 */ 
bool_t makeAction(int8_t output){
    switch (output)
    {
    case 4:
        gpioWrite(LED1,ON);
        delay(1000);
        gpioWrite(LED1,OFF);
        break;
    case 5:
        gpioWrite(LED2,ON);
        delay(1000);
        gpioWrite(LED2,OFF);
        break;
    default:
        break;
    }
    return true; ///< Ok return.
};