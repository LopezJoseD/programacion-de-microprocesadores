/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/16
 * Version: 1
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "aprendiendo.h"
#include "sapi.h"      // <= sAPI header

/*==================[macros and definitions]=================================*/

int main( void )
{
    // Inicializar la placa
   boardConfig();
   adcConfig( ADC_ENABLE ); /* ADC */

   delay(500);
   printf("Semaforo inicializado \r\n");
   gpioWrite(LED1,1);

   gpioWrite(44,1);
   gpioInit(T_COL0,GPIO_OUTPUT);
   
   int sentido = 0;
   /* Variable para almacenar el valor leido del ADC CH1 */
   uint16_t muestra = 0;
   float convertido = 0;

   while(true){

      muestra = adcRead( CH1 );
      convertido = muestra*3.3/1024;
      printf("El ADC lee: %d que eso equivale a: %f\r\n",muestra,convertido);
      delay(500);
   }
   return 0;
}