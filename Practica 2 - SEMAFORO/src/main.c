/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/18
 * Version: 1
 *===========================================================================*/

#include <semaforo.h>

/*=====[Main function, program entry point after power on or reset]==========*/

int main( void )
{
   semaphore_t sem1; ///< creation of new traffic lights
   boardInit(); ///< Board initialization
   
   if (semaphoreInit(&sem1)){ ///< If traffic lights is successfully initialized we prent ok message
       printf("Se inicializa correctamente el semaforo \r\n"); ///< traffic light ok message
   }else {
       printf("Problema inicializando el semaforo \r\n"); /// traffic light not ok
       printf("Enciendo la alarma \r\n"); ///< error message
       return 0;
   }

   // ----- Repeat for ever -------------------------
   while( true ) {
      semaphoreControl(&sem1);
   }
   return 0;
}