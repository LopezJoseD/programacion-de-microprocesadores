/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/18
 * Version: 1
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "semaforo.h"
#include "sapi.h"
#include "led.h"

/*=====[Definition macros of private constants]==============================*/
#define INITIAL_DEFAULT_MODE ALARM
#define INITIAL_DEFAULT_STATE YELLOWL
#define AMARILLO LED1 
#define ROJO LED2
#define VERDE LED3
/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
delay_t retardo;
int8_t i = 3;
int8_t seq_steps = 0;

static void semaphore_normal(semaphore_t * sem){
   switch (i){
      case 1:
         if (seq_steps != i){
            turnOn(ROJO);
            delayWrite(&retardo, 3000 );
            seq_steps = i;
         }else {
            if (delayRead(&retardo)){
               turnOff(ROJO);
               i++;
            }
         }
         break;
      case 2:
         if (seq_steps != i){
            turnOn(VERDE);
            delayWrite(&retardo, 1000 );
            seq_steps = i;
         }else {
            if (delayRead(&retardo)){
               turnOff(VERDE);
               i++;
            }
         }
         break;
      case 3:
         if (seq_steps != i){
            turnOn(AMARILLO);
            delayWrite(&retardo, 500 );
            seq_steps = i;
         }else {
            if (delayRead(&retardo)){
               turnOff(AMARILLO);
               i=1;
               printf("Soy un semaforo normal funcionando \r\n");
            }
         }
         break;
   }
};
static void semaphore_offline(semaphore_t * sem){
   if (i==1){
      if (seq_steps != i){
         toggle(AMARILLO);
         delayWrite(&retardo, 500 );
         seq_steps = i;
      }else {
         if (delayRead(&retardo)){
            i++;
         }
      }
   }else {
      if (seq_steps != i){
         toggle(AMARILLO);
         delayWrite(&retardo, 500 );
         seq_steps = i;
      }else {
         if (delayRead(&retardo)){
            i=1;
            printf("Soy un semaforo offline funcionando \r\n");
         }
      }
   }
};
static void semaphore_alarm(semaphore_t * sem){
   if (i==1){
      if (seq_steps != i){
         turnOn(ROJO);
         turnOff(AMARILLO);
         delayWrite(&retardo, 1000 );
         seq_steps = i;
      }else {
         if (delayRead(&retardo)){
            i++;
         }
      }
   }else {
      if (seq_steps != i){
         turnOn(AMARILLO);
         turnOff(ROJO);
         delayWrite(&retardo, 1000 );
         seq_steps = i;
      }else {
         if (delayRead(&retardo)){
            i=1;
            printf("Soy un semaforo alarm funcionando \r\n");
         }
      }
   }
};

void cleanLights(){
   seq_steps = 0;
   i = 1;
   turnOff(VERDE);
   turnOff(AMARILLO);
   turnOff(ROJO);
}

bool_t semaphoreInit(semaphore_t * pSemaphore){
   
   if (NULL == pSemaphore){
      turnOn(LEDR);
      printf("There is no semaphore received to init \r\n");
      return false;
   }
   turnOn(LEDB);
   cleanLights();
   pSemaphore->mode = INITIAL_DEFAULT_MODE;
   pSemaphore->status = INITIAL_DEFAULT_STATE;
   
   return true;
};

bool_t semaphoreControl(semaphore_t * pSemaphore){
   if (NULL == pSemaphore){
      return false;
   }

   if (!gpioRead(TEC1)){
      pSemaphore->mode = NORMAL;
      cleanLights();
   }else if (!gpioRead(TEC2)){
      pSemaphore->mode = OFFLINE;
      cleanLights();
   }else if (!gpioRead(TEC3)){
      pSemaphore->mode = ALARM;
      cleanLights();
   }

   switch (pSemaphore->mode){
      case NORMAL:
         semaphore_normal(pSemaphore);
         break;
      case OFFLINE:
         semaphore_offline(pSemaphore);
         break;
      case ALARM:
         semaphore_alarm(pSemaphore);
         break;
      default:
         semaphoreInit(pSemaphore);
   }

   return true;
};

