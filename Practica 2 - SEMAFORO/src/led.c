/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/18
 * Version: 1
 *===========================================================================*/

#include <led.h>
#include <sapi.h>

void turnOn(lights_t lamp){
    gpioWrite(lamp,ON);
};
void turnOff(lights_t lamp){
    gpioWrite(lamp,OFF);
};
void blink(lights_t lamp, tick_t blinkTime){
    gpioWrite(lamp,ON);
    delay(1000);
    gpioWrite(lamp,OFF);
};

void toggle(lights_t lamp){
    gpioToggle(lamp);
}