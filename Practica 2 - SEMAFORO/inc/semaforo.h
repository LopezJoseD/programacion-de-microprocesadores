/*=============================================================================
 * Copyright (c) 2020, José Daniel López <josedlopez11@gmail.com>
 * All rights reserved.
 * License: bsd-3-clause (see LICENSE.txt)
 * Date: 2020/03/18
 * Version: 1
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __SEMAFORO_H__
#define __SEMAFORO_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include <stdint.h>
#include <stddef.h>
#include <sapi.h>

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/
typedef enum {
    NORMAL, 
    OFFLINE, 
    ALARM
} semaphore_mode_t;

typedef enum {
    GREENL,
    YELLOWL,
    REDL
} semaphore_state_t;

typedef struct {
    semaphore_mode_t mode;
    delay_t delay;
    semaphore_state_t status;
} semaphore_t;



/*=====[Prototypes (declarations) of public functions]=======================*/
bool_t semaphoreInit(semaphore_t * pSemaphore);
bool_t semaphoreControl(semaphore_t * pSemaphore);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __SEMAFORO_H__ */
