#ifndef __LEDS_H__
#define __LEDS_H__
#include <sapi.h>

typedef gpioMap_t lights_t;

void turnOn(lights_t lamp);
void turnOff(lights_t lamp);
void blink(lights_t lamp, tick_t blinkTime);
void toggle(lights_t lamp);


#endif /* __LEDS_H__ */